#Càlcula l'error per a un model aleatòri

library(data.table)

SFtrain <- fread("../data/train.csv")

vals <- summary(as.factor(SFtrain$Category))
vals_norm <- sort(vals/sum(vals), decreasing = TRUE)
cats <- names(vals_norm)

vals2 <- c()
for(i in 1:length(vals_norm)){
    vals2[i] <- sum(as.numeric(vals_norm[1:i]))
}

randnums <- runif(nrow(SFtrain))

catsrand <- rep(0, length.out = length(randnums))
for(i in 1:length(randnums)){
  catsrand[i] <- cats[1]
  for(j in 1:(length(vals2)-1)){
    if(randnums[i] >= vals2[j] && randnums[i] <= vals2[j+1]){
      catsrand[i] <- cats[j+1]
      break
    }
  }
}

j <- 0
for(i in 1:length(randnums)){
  if(SFtrain$Category[i] == catsrand[i])
    j <- j+1
}

print(paste("Error:", 1-j/length(randnums)))
      